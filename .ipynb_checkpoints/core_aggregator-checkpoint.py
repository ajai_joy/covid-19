# Import the Pandas library
import pandas as pd
import uuid

# The files from 01-22-2020 to
#
#

jan_file_paths = [
    "01-22-2020.csv",
    "01-23-2020.csv",
    "01-24-2020.csv",
    "01-25-2020.csv",
    "01-26-2020.csv",
    "01-27-2020.csv",
    "01-28-2020.csv",
    "01-29-2020.csv",
    "01-30-2020.csv",
    "01-31-2020.csv"
]
feb_file_paths = [
    "02-01-2020.csv",
    "02-02-2020.csv",
    "02-03-2020.csv",
    "02-04-2020.csv",
    "02-05-2020.csv",
    "02-06-2020.csv",
    "02-07-2020.csv",
    "02-08-2020.csv",
    "02-09-2020.csv",
    "02-10-2020.csv",
    "02-11-2020.csv",
    "02-12-2020.csv",
    "02-13-2020.csv",
    "02-14-2020.csv",
    "02-15-2020.csv",
    "02-16-2020.csv",
    "02-17-2020.csv",
    "02-18-2020.csv",
    "02-19-2020.csv",
    "02-20-2020.csv",
    "02-21-2020.csv",
    "02-22-2020.csv",
    "02-23-2020.csv",
    "02-24-2020.csv",
    "02-25-2020.csv",
    "02-26-2020.csv",
    "02-27-2020.csv",
    "02-28-2020.csv",
    "02-29-2020.csv"]

# utility functions
def cleanse_data(covid_date_df):
    return covid_date_df.dropna(
        subset=['Province/State', 'Country/Region', 'Last Update', 'Confirmed'])

file_paths = jan_file_paths + feb_file_paths
master_dfs = []

# stage_1
for index in range(0, len(file_paths), 1):
    file_path = "data/raw/csse_covid_19_daily_reports/" + file_paths[index]
    # print(file_path)

    temp_df = pd.read_csv(file_path)
    # inject unique id guid
    temp_df['unique_id'] = [uuid.uuid4() for _ in range(len(temp_df.index))]

    # write and check if guid is injected
    temp_df.to_csv("data/stage_1/test" + str(index) + ".csv", index=False, header=True)

    no_null_covid_date_df = cleanse_data(temp_df)
    master_dfs.append(no_null_covid_date_df)

print(f"Total dataframes is {len(master_dfs)}")

# stage_2
post_concat_df = pd.concat(master_dfs, join='outer', ignore_index=False, keys=None,
                           levels=None, names=None, verify_integrity=False, copy=True)

post_concat_df.to_csv("data/stage_2/master-covid.csv", index=False, header=True)



