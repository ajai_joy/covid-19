# %%
# This python file is the one that reads the data from master-covid.csv and population data that cleansed.
# This python file is the one that creates inights for the COVID confirmed cases and the BCG.

# read the master-covid file
# Import the Pandas library
import pandas as pd
import uuid


import pandas as pd
import numpy as np

from matplotlib import style

import matplotlib.pyplot as plt
#%matplotlib inline


# %%
# File to load
file_path = "data/stage_2_2-final/master-covid-v2-final.csv"
# print(file_path)

master_covid_df = pd.read_csv(file_path, low_memory=False)
master_covid_df.head()


# %%
master_covid_df['Country'] = master_covid_df['Country'].replace({'China':'Mainland China'})
#master_covid_df['Country'] = master_covid_df['Country'].replace({'US':'Brazil'})
master_covid_df['Country'] = master_covid_df['Country'].replace({'France':'Colombia'})
#master_covid_df['Country'] = master_covid_df['Country'].replace({'Canada':'India'})
#master_covid_df['Country'] = master_covid_df['Country'].replace({'Columbia':'Colombia'})
master_covid_df['Country'] = master_covid_df['Country'].replace({'Taiwan':'Viet Nam'})
group_confirmed_df = master_covid_df.groupby(['Country'],as_index=False)["Confirmed"].count()
group_confirmed_df.head(30)

# %%


# %%
# File to load
file_BCG_path = "data/stage_4/BCG_Population_Cleansed.csv"
BCG_covid_df = pd.read_csv(file_BCG_path)
BCG_covid_df.head(30)

# %%
# Renaming the BCG

BCG_covid_df.head()
BCG_covid_df['Country/Region'] = BCG_covid_df['Country/Region'].replace({'China':'Mainland China'})
BCG_covid_df.head(30)

# %%
#Since USA doesn't BCG vacination, we are adding 0% for USA population
new_row = {'Country/Region':'US', '2018':'10.0', 'Population':'328,000,000' }
BCG_covid_df = BCG_covid_df.append(new_row, ignore_index=True)
BCG_covid_df['2018 Int']=pd.to_numeric(BCG_covid_df['2018'])

BCG_covid_df['Population'] = BCG_covid_df['Population'].str.replace(',','') 

BCG_covid_df['2018 Pop Int'] = pd.to_numeric(BCG_covid_df['Population'])

BCG_covid_df['bcg_vaccinated'] =round((BCG_covid_df['2018 Int']/100)* BCG_covid_df['2018 Pop Int'],2)


#del BCG_covid_df['2018']
#del BCG_covid_df['Population']
#del BCG_covid_df['2018 Int']
#del BCG_covid_df['2018 Pop Int']
bcg_df = BCG_covid_df.rename(columns={'Country/Region': 'Country'})
bcg_df.head(10)

bcg_df.index = bcg_df['Country']
del bcg_df['2018']
del bcg_df['Population']
del bcg_df['2018 Int']
del bcg_df['2018 Pop Int']
del bcg_df['Country']
bcg_df.head(10)

# %%


# %%

group_confirmed_df.columns=['Country','COVID_Confirmed']
group_confirmed_df.index = group_confirmed_df['Country']
del group_confirmed_df['Country']
group_confirmed_df.head(1000)

# %%


# Merge Dataset and create country as index column
merge_df = pd.merge(group_confirmed_df, bcg_df, on="Country")
merge_df



# %%
#Plot the graph
fig = plt.figure() 
ax = fig.add_subplot(111) # Create matplotlib axes
ax2 = ax.twinx() # Create another axes that shares the same x-axis as ax.
width = 0.4
merge_df.COVID_Confirmed.plot(kind='bar', color='red', figsize=(10, 5), ax=ax, width=width,position=1)
merge_df.bcg_vaccinated.plot(kind='bar', color='blue', figsize=(10, 5), ax=ax2, width=width, position=0)
ax.set_ylabel('COVID Confimed')
ax2.set_ylabel('BCG Vaccinated')
plt.show()


# %%
import seaborn as sns
%matplotlib inline



sns.heatmap(merge_df, cmap="PiYG")
plt.savefig("Heatmap.png")
plt.show()


# %%


# %%


# %%
