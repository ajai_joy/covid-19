# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
 Raw data source : https://github.com/CSSEGISandData/COVID-19

 The core aggregator performs the following functions:

 1. It reads data from data/raw/csse_covid_19_daily_reports which has daily files for covid 19 cases.
 2. The format of the files have changed after 4-2-2020, the covid data is available from Jan 22 2020 to May 4 2020(end of project)
 3. The individual dated files are read on a monthly basis as csv files and temp data frames are created, then a unique
 id is injected for each record, we pump data onto disk at this in stage_1
 4. The next phase in the datapipeline is to cleanse the data ie dropna for selected listed of columns. We cant use
 all columns since having no deaths is still a valid case.
 5. The def cleanse_data(covid_date_df): handles both formats of the file for cleansing
 6. The data/stage_2/master-covid.csv is where  concat of individual files is done, a concat is a better fit here than
 merge because there are no joins since the data is an extension and not discreete data sets
 7. In stage2_1, stage2_2 columns are renamed, merged (ex: Country_Region, Country/Region since formats are changed)
 8. This is saved in stage 2_1, stage2_2 after selecting desired columns with the curated data.
 9. stage_3 is for BCG data and stage_4 is for population data.
 Please refer to README.md for more details

* Version
0.1

### Architecture ###

![Image description](Covid-19%20datapipeline%20architecture.jpeg)


### Code ###
1. core_aggregator does the covid-19 data ingestion.
2. bcg_ingester does the bcg vaccination data ingestion
3. insight_generator generates insights based on <1> & <2>


The code is in python and is both .py and .pynb files are used and kept in sync using

  ipynb-py-convert core_aggregator.py core_aggregator.ipynb
  ipynb-py-convert bcg_ingester.py bcg_ingester.ipynb
  
  ipynb-py-convert core_aggregator.pynb core_aggregator.py
  ipynb-py-convert bcg_ingester.pynb bcg_ingester.py
  
### Preliminary Findings ###

![Image description](covid-19vsBCG.png)

![Image description](scatter.png)
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* ajai.joy@gmail.com