# This file reads the BCG vacination data from the WHO website,filters the data for countries and year =2018
# Later we filter to eight candidate countries, and later merge with the Population data set.


# Import the Pandas library
import pandas as pd
import numpy as np
import uuid

# File to load
csv_path = "data/raw/BCG/BCG.csv"
print(csv_path)


# Data derived from WHO data available at <https://www.who.int/immunization/monitoring_surveillance/routine/coverage/en/index4.html>


# Read with Pandas
BCG_df = pd.read_csv(csv_path)
BCG_df.head()


# Because 2018 column shows estimated coverage for that year, discard all rows except 2018
filtered_BCG_df = BCG_df.loc[:, ["Country/Region", "2018"]]
filtered_BCG_df.head()



# Dropping rows where any column is missing
filtered_BCG_df = filtered_BCG_df.dropna(axis = 0, how = 'any')
filtered_BCG_df.head()


# Narrow down to eight candidate countries
# Brazil, China, Colombia, India, Japan, Nigeria, Philippines, Vietnam
country = {'Country/Region': ['Brazil','China','Colombia','India', 'Japan', 'Nigeria', 'Philippines (the)', 'Viet Nam']}
df = pd.DataFrame(country)

merged_df = pd.merge(filtered_BCG_df, df, on = "Country/Region")
merged_df.to_csv("data/stage_3/BCG_Cleansed.csv", index=False, header=True)
print(merged_df)


# Merge Population.csv for the eight candidate countries
pop_path = "data/raw/population/Population.csv"
pop_df = pd.read_csv(pop_path)
merge_df2 = pd.merge(merged_df, pop_df, on = "Country/Region")
print(merge_df2)

# Data derived from CIA World Factbook data and is current as of 2020


# create a file with above-mentioned datasets
merge_df2.to_csv("data/stage_4/BCG_Population_Cleansed.csv", index=False, header=True)


